console.log("it works!");

class Person {
    name: string;
    protected age: number = 25;
    private type: string;

    constructor(name: string, public username: string) {
        this.name = name;
    }

    private static printLine(line: string) {
        console.log(line);
    }

    printAge(): void {
        console.log(this.age);
    }

    setType(type: string) {
        this.type = type;
        console.log(this.type);

        Person.printLine(this.type);
    }
}

class Max extends Person {
    //name = "Max";

    constructor(username: string) {
        super("Max", username);
        this.age = 31;

        //console.log(this.type);  // NOT WORKING - type is private
    }
}

//const person: Person = new Person();
const person = new Person("Max", "max");
console.log(person);
console.log(person.name);

person.printAge();
person.setType("Cool guy");

const maxPerson = new Max("max");
console.log(maxPerson);


// GETTERS AND SETTERS
class Plant {
    private _species: string = "Default";

    get species() {
        return this._species;
    }

    set species(value: string) {
        if ( value.length > 3 ) {
            this._species = value;
        }
        else {
            this._species = "Default";
        }
    }
}

const plant = new Plant();
console.log(plant.species);

plant.species = "AB";
console.log(plant.species);

plant.species = "ABCD";
console.log(plant.species);


// STATIC PROPERTIES AND METHODS
class Helpers {
    static PI: number = 3.14;

    static calcCircumference(diameter: number): number {
        return this.PI * diameter;
    }
}

console.log("Static example ", 2 * Helpers.PI);
console.log("Static example ", Helpers.calcCircumference(5));


// ABSTRACT CLASSES
abstract class Project {
    projectName: string = "Default";
    budget: number = 1000;

    abstract changeName(name: string): void;

    calcBudget() {
        return this.budget * 2;
    }
}

class ITProject extends Project {

    changeName(name: string): void {
        this.projectName = name;
    }
}

const project = new ITProject();
console.log(project);

project.changeName("Super IT project");
console.log(project);
console.log(project.calcBudget());


// private constructors - SINGLETON
class OnlyOne {
    private static instance: OnlyOne;

    // readonly can also be achieved with using only getter without setter and private variable field
    private constructor(public readonly name: string) {
    }

    static getInstance() {
        if ( !OnlyOne.instance ) {
            OnlyOne.instance = new OnlyOne("The only one");
        }
        return OnlyOne.instance;
    }
}

//const wrong = new OnlyOne("The only one");
const right = OnlyOne.getInstance();
console.log(right);
//right.name = "Something else ";
