"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
console.log("it works!");
var Person = /** @class */ (function () {
    function Person(name, username) {
        this.username = username;
        this.age = 25;
        this.name = name;
    }
    Person.printLine = function (line) {
        console.log(line);
    };
    Person.prototype.printAge = function () {
        console.log(this.age);
    };
    Person.prototype.setType = function (type) {
        this.type = type;
        console.log(this.type);
        Person.printLine(this.type);
    };
    return Person;
}());
var Max = /** @class */ (function (_super) {
    __extends(Max, _super);
    //name = "Max";
    function Max(username) {
        var _this = _super.call(this, "Max", username) || this;
        _this.age = 31;
        return _this;
        //console.log(this.type);  // NOT WORKING - type is private
    }
    return Max;
}(Person));
//const person: Person = new Person();
var person = new Person("Max", "max");
console.log(person);
console.log(person.name);
person.printAge();
person.setType("Cool guy");
var maxPerson = new Max("max");
console.log(maxPerson);
// GETTERS AND SETTERS
var Plant = /** @class */ (function () {
    function Plant() {
        this._species = "Default";
    }
    Object.defineProperty(Plant.prototype, "species", {
        get: function () {
            return this._species;
        },
        set: function (value) {
            if (value.length > 3) {
                this._species = value;
            }
            else {
                this._species = "Default";
            }
        },
        enumerable: true,
        configurable: true
    });
    return Plant;
}());
var plant = new Plant();
console.log(plant.species);
plant.species = "AB";
console.log(plant.species);
plant.species = "ABCD";
console.log(plant.species);
// STATIC PROPERTIES AND METHODS
var Helpers = /** @class */ (function () {
    function Helpers() {
    }
    Helpers.calcCircumference = function (diameter) {
        return this.PI * diameter;
    };
    Helpers.PI = 3.14;
    return Helpers;
}());
console.log("Static example ", 2 * Helpers.PI);
console.log("Static example ", Helpers.calcCircumference(5));
// ABSTRACT CLASSES
var Project = /** @class */ (function () {
    function Project() {
        this.projectName = "Default";
        this.budget = 1000;
    }
    Project.prototype.calcBudget = function () {
        return this.budget * 2;
    };
    return Project;
}());
var ITProject = /** @class */ (function (_super) {
    __extends(ITProject, _super);
    function ITProject() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ITProject.prototype.changeName = function (name) {
        this.projectName = name;
    };
    return ITProject;
}(Project));
var project = new ITProject();
console.log(project);
project.changeName("Super IT project");
console.log(project);
console.log(project.calcBudget());
// private constructors - SINGLETON
var OnlyOne = /** @class */ (function () {
    // readonly can also be achieved with using only getter without setter and private variable field
    function OnlyOne(name) {
        this.name = name;
    }
    OnlyOne.getInstance = function () {
        if (!OnlyOne.instance) {
            OnlyOne.instance = new OnlyOne("The only one");
        }
        return OnlyOne.instance;
    };
    return OnlyOne;
}());
//const wrong = new OnlyOne("The only one");
var right = OnlyOne.getInstance();
console.log(right);
//right.name = "Something else ";
